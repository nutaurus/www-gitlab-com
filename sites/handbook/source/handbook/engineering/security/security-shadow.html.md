---
layout: handbook-page-toc
title: "Security Shadow Program"
---

### On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Security Shadow Program
From converging on real-time critical events with SIRT, exploiting vulnerabilities with the Red Team or participating in live Customer Assurance calls with the Risk and Field Security team, you will have the opportunity to work next to security staff to gain valuable insight and working knowledge of security fundamentals across multiple domains.

## Course Catalog
Our course catalog is divided into three main areas in alignment with the three [Sub-Organizations](https://about.gitlab.com/handbook/engineering/security/#department-structure) within the Security Department. 

- [Security Engineering and Research](sites/handbook/source/handbook/engineering/security/security-shadow-sec-eng-res.html.md)
- [Security Operations](sites/handbook/source/handbook/engineering/security/security-shadow-security-operations.html.md)
- [Security Assurance](sites/handbook/source/handbook/engineering/security/security-shadow-security-assurance.html.md)

## Enrollment

If you're ready to embark on this adventure with us, your first step is to talk to your manager. Make sure they approve the time you will need to dedicate to this program. 

With their approval, complete this [Google From](https://docs.google.com/forms/d/e/1FAIpQLSdcrMt_FO385SoJmWZrP-pvnybsPOo5r725DvOPRep5vQxv5w/viewform?usp=sf_link) and a member of the Security Team will reach out to schedule your sessions. 

## Questions
Feel free to contact the Security Team in our [Slack Channel](https://gitlab.slack.com/archives/CM74JMLTU) 


 
