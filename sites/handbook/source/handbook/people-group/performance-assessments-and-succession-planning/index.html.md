---
layout: handbook-page-toc
title: Performance/Potential Matrix and Succession Planning
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview

There are many Talent Assessment Options, we have opted to use a Performance/Potential Matrix (commonly known as "9-Box" in the US). GitLab's Performance/Potential Matrix is a type of Talent Assessment that forms part of our [Talent Development Program](https://about.gitlab.com/handbook/people-group/talent-development-program/). 

To ensure we remain efficient, we built a [Performance/Potential Assessment Tool](https://about.gitlab.com/handbook/people-group/engineering/assessment-tool/), this is currently [(CY20-Q3 / FY21-Q3)](/handbook/communication/#writing-style-guidelines) being tested by a few teams, to provide feedback and iteration prior to launching company-wide.   


# The Performance/Potential Matrix

## Performance/Potential Matrix (Leadership)

| | | | |
|-----------------|---------------|---------------|----------------|
| **Exceeding** | Good current performance but with risk of inability to grow leadership capabilities, expertise and future focus as required by the business; focus is on developing leadership capability with emphasis on | Strong business performance and strong day to day management of the business/function; management style is focused on today’s challenges; focus is on developing leadership capability with emphasis on innovation | Exceptional performance coupled with strong leadership capability to drive innovative growth of business; focus is on leveraging against critical needs of business |
| **Performing** | Solid business performance today but with risk of falling behind due to lack of leadership capability | Solid management and performance for today’s business challenges; potential to maximize contribution in current business model; focus on development of leadership capabilities and maintaining required expertise | Solid Performance with high potential to make significant contributions to the business; positioned to excel by leveraging leadership capabilities in challenging assignments |
| **Developing** | Below average performance and inconsistent leadership behaviors; operating from previous business models despite feedback focus is on performance management | Average to below average performance with limited capacity to increase performance through management and leadership capabilities; focus is on evaluating fit in the role; development is on improving performance | High Potential leader who is underperforming due to newness of role, business strategy or management team: focus is on enhancing contribution and leveraging leadership capabilities |
| Performance ↑ Potential → | **Low Potential** | **Medium Potential**| **High Potential**|

## Performance/Potential Matrix (Individual Contributor)

| | | | |
|-----------------|---------------|---------------|----------------|
| **Exceeding** | Seasoned professional capable of expanded role, but maybe be experiencing problems that requires coaching and mentoring. | Does extremely well at current job with potential to do more; give stretch assignments to help prepare for next level. | Consistently performs well in a variety of assignments. Big picture thinker, aligns to our values, problem solver and self motivated. Focus on delegating to them or providing development opportunities |
| **Performing** | With coaching, could progress within same level; focus on stretch goals for this team member. | May be considered for job enlargement at the same level but may need coaching in several areas, including people management. | Current role may still provide opportunity for growth/ development; Strong tactically. Focus on helping them improve strategic thinking.  |
| **Developing** | May be a candidate for reassignment, reclassification to a lower level or exit the organization.  | Effective performer, but may have reached career potential; coach team member on becoming more innovative and focus on lateral thinking. | Experienced high performer but reached limit in career potential. Still a valueable team member. Focus on encouraging development of communications and delegation skils. |
| Performance ↑ Potential → <b> | **Low Potential** | **Medium Potential**| **High Potential**|

This matrix is an individual assessment tool that evaluates both a team members current contribution to the organization and their potential level of contribution.
It is commonly used in succession planning as a method of evaluation an organization's talent pool, identifying potential leaders, and identifying any gaps or risks.
It is regularly considered a catalyst for robust dialogue and is considered more accurate than one person's opinion.
The performance/potential matrix can be a diagnostic tool for development.

### Performance/Potential High Level

Below are examples for Managers to consider when compiling their criteria set, some of which speak to productivity and others to potential giving a balanced sense of where the team member should be placed in the matrix:

| Criteria | Points for Consideration |
| --- | --- | 
| **Output Delivery** | Is the team member consistently able to produce adequate results within the predefined scope of their role - consider this in parallel with applicable Job Family. |
| **Solution Centricity** | Is the team member able identify solutions and achieve results in an innovative way, is there an awareness of over-arching company strategies and are they contributing in some way to the achievement of them. |
| **Core Value Alignment** | Does the team member consistently demonstrate the core [GitLab Values](https://about.gitlab.com/handbook/values/), always operating within the best interests of the company and those who work in it. |
| **Self-Directed Career Mobility** | is the team member able to learn and adapt quickly - applying the knowledge gained to overcoming challenging situations and achiving the desired results. |
| **Leadership Competencies** | Does the team member exhibit the [competencies](https://about.gitlab.com/handbook/competencies/#manager-and-leadership-competencies) required to become a Manager / Leader within GitLab and if so do they require further development in these areas. |
| **Promotion Readiness** | Is the team member ready to move into a more senior role within GitLab - if not what would you consider the time frame for development to be. |
| **Team Dynamics** | Does the team member collaborate well with other team members, peers, and stable counterparts? Are they someone people are happy to work with? Data sources for this information can be a variety of things, ranging from 360 feedback, observations in team meetings, 1:1 conversation/feedback, etc.|

We elaborate on these points below when breaking providing a more in depth overview of performance measurement criteria and potential measurement criteria. 

## What is "Performance"?

Performance is broken into three areas: Developing, Performing, and Exceeding.

### Developing

Around 10-15% of the team generally fall into the “Developing” category. Based on the Job Family responsibilities, CREDIT values and remote working competencies, team members in this category are not meeting all of the expectations. There is still more to learn and improve on in the current position to achieve the desired results. This may be due to the following: 

- The team member has not been in the position/at GitLab long enough to adequately demonstrate performance.
- The team member has been recently promoted and needs time to ramp up performance in the new role. 
- The team member may have lost pace with changes in the organization. 
- The team member is not committed to the job responsibilities and/or GitLab. 
- The team member finds it difficult to perform work in a way that is aligned with our CREDIT values and/or competencies. 
- The team member's performance is aligned to our values and competencies, however, they lack results for Job Family responsibilities. 
- Team member was part of a recent [transfer](https://about.gitlab.com/handbook/people-group/promotions-transfers/#department-transfers) and is still learning how to be successful on their new team.

Examples: 

- The team member needs a lot of guidance from the manager with the majority of tasks to understand requirements and deliverables expected.
- The team member is missing important due dates which is affecting the team, GitLab and/or customers. 
- The team member still needs to adapt remote working best practices. For example, the team member is not responsive. 
- The team member lacks (a part of) the knowledge, skills and abilities which are required for the role, resulting in a low quality of work being delivered.
- There is a significant mis-alignment (I.E. The team member struggles to collaborate with others, the team member does not work iteratively and key metrics are impacted, etc.) 

### Performing

Most team members (60-65%) fall into the “Performing” category. Based on Job family responsibilities, CREDIT values and remote working competencies team members in this category are “on track” and meeting all expectations. They can independently and competently perform all aspects of the job family responsibilities in a way that is aligned with our CREDIT values and competencies. Their performance consistently meets the requirements, standards, or objectives of the job, and they can occasionally exceed expectations. They deliver results in a timely and accurate fashion. This performance may be expected as a result of: 

- The team member is consistent and stable in their performance.  
- The team member has the required knowledge, skills and abilities to perform in the role successfully and also apply CREDIT values and competencies in their work.
- The team member has adapted to remote working and their knowledge, skills, and abilities align seamlessly to their role at GitLab. 

Examples: 

- While consistently meeting expectations, the team member does not regularly exceed expectations.
- Team member does not frequently express interest in additional projects, responsibilities, or work outside of their immediate scope.
- The team member is exemplifying our values in their work, but might have 1-2 competencies to improve on. 

### Exceeding

A relatively small portion of team members (about 25%) generally fall into the “Exceeding” category. Team members that are exceeding consistently surpass the demands of their current position and/or division. They demonstrate unique understanding of work beyond the assigned area of responsibility. They contribute to GitLab’s success by adding significant value well beyond job requirements, CREDIT values, and competencies. This performance may be expected as a result of: 

- Individual is developing more quickly than the demands of their current position. 
- They rise to the challenge when give additional assignments and achieves significant results.
- A “go-to” team member for their peers for tough problems and day-to-day guidance.
- High commitment and engagement to GitLab combined with extensive knowledge, skills, and abilities to perform in the role. 

Examples: 

- Constantly seeks opportunities to improve both self and organization. 
- The team members exemplify great ways of giving and receiving feedback - incorporating this directly in their work. 
- The team members are ambassadors for the CREDIT values, take full ownership of delivering projects, and very rarely miss due dates. 

### The Performance Factor

While the primary objective of the performance axis of the Performance/Potential Matrix is to calibrate team member performance, this axis of the matrix also directly impacts the [Performance Factor](/handbook/total-rewards/compensation/compensation-review-cycle/#performance-factor), which is a determining factor in the annual compensation review. Please reference the [Total Rewards Performance Factor page](/handbook/total-rewards/compensation/compensation-review-cycle/#performance-factor) for more detail on compensation impact.

### Measuring Performance

How can managers determine whether their team members are Developing, Performing, or Exceeding? "The Performance Factor" = Job Family Responsibilities (60%) + Competencies (40%). It is _optional_ to use [The Performance Factor Workheet](https://docs.google.com/spreadsheets/d/1HHe-Vb6y6F4HXsek3sObV4IVNAGaTvXGi_9fbatT0Uo/edit#gid=241716076) for weighing the different Performance Factors.


#### Job Family

Performance against job family role, requirements, and performance indicators should be weighted at 60%. 

* Review Job Families: Look at the Responsibilities section/how this previously was feeding into Compa Groups for the roles in your department.
    * Example: [Software Engineer in Test](https://about.gitlab.com/job-families/engineering/software-engineer-in-test/#responsibilities)
* Review Performance indicators per Job Family: Look at the Performance indicators.
    * Example: [Software Engineer in Test](https://about.gitlab.com/job-families/engineering/software-engineer-in-test/#performance-indicators)

For management roles, please also review and consider the [levels](https://about.gitlab.com/company/team/structure/#levels) outlined in the Organizational Structure. 

#### Competencies

Performance against competencies should be weighted at 40%. 

* [Values Competencies](https://about.gitlab.com/handbook/competencies/#values-competencies)
* [Remote Working Competencies](https://about.gitlab.com/handbook/competencies/#remote-work-competencies)
* _(if applicable)_ [Functional Competenceis](https://about.gitlab.com/handbook/competencies/#functional-competencies) 
* _(if applicable)_ [Manager and Leadership Competencies](https://about.gitlab.com/handbook/competencies/#manager-and-leadership-competencies) 

## What is "Potential"?

Potential refers to the ability and desire of a team member to successfully assume increasingly broad or complex responsibilities and scale with the company, as compared to peers and the roles and responsibilities outlined in their respective job family. 

This could include a move up to the next level, and/or a lateral move. 

_We will elaborate on the criteria to assess potential in our next iteration by 2020-10-12._

## Calibration Session Guidelines

The portion of the Performance/Potential matrix that often entails the most significant time commitment is the live calbiration session of team members with leadership. The calibration session is very valuable to ensure consistency across the job family and level, raise any questions, and provide cross-departmental and/or cross-divisional feedback on team members to capture the assessment of different managers as opposed to the opinion of the direct manager exclusively. 

### Pre Work

It is **absolutely essential** that managers complete the required pre-work to ensure that the live calibration sesison is as efficient and productive as possible. Pre work includes:

- Review the job family/families that will be reviewed in detail
- Review our competencies 
    - [Values competencies](/handbook/competencies/#values-competencies)
    - [Remote work competencies](/handbook/competencies/#remote-work-competencies)
    - [Functional competencies](/handbook/competencies/#functional-competencies) _(if applicable)_
- Review our [Unconscious Bias](https://about.gitlab.com/company/culture/inclusion/unconscious-bias/) handbook page and consider watching our [Recognizing Bias Training](https://about.gitlab.com/company/culture/inclusion/unconscious-bias/#recognizing-bias-training) if you haven't already. 
- Review the [Performance/Potential Matrix Training Information](https://docs.google.com/presentation/d/151ys8xkOak9ifU9IPXQydZ44sb_BoMpMocWmjVonLHE/edit), which delves into definitions for each box. 
- Determine the appropriate box for each of your team members
- Add notes for each of your team members to the agenda 
    - An example of notes could include:
        - 2-3 strengths
        - 2-3 improvement areas
        - Anything else noteworthy (I.E. recently promoted, COVID impact, etc.)
- Review the notes of your peers for other team members and add feedback/questions/thoughts for discussion

### Calibration Session

The calibration session is one of the most important pieces of the Performance/Potential Matrix process, as it provides time for managers, their peers, and their manager to calibrate. Guidelines to ensure efficiency during calibration sessions are:
* It may not be needed to discuss every team member in detail. Calibration sessions typically focus on "surprises". 
* Be conscious of time and consider setting a (reasonable) time limit per team member being discussed. 
* **Ask questions and provide feedback!** This may seem obvious, but it is import aspect to help identify any potential bias
* Leaders should feel comfortable highlighting team members in other functions

## Matrix Tool

Our goal is to have the whole company use the the Matrix [Assessment Tool](https://about.gitlab.com/handbook/people-group/engineering/assessment-tool/) for the Performance/Potential matrix. However, while the tool is being developed and tested, groups who want to get a head start on the Performance/Potential matrix can use the [Performance/Potential Matrix team template](https://docs.google.com/presentation/d/123fS20lqXt-bhDEqwMCyL2K1BAcgVCEd61pQW6FmfRw/edit#slide=id.g817046ecd3_0_44).

## Identifying Action 

There is an overview of recommended actions to take after the calibration session included in the [Performance/Potential Training Information](https://docs.google.com/presentation/d/151ys8xkOak9ifU9IPXQydZ44sb_BoMpMocWmjVonLHE/edit#slide=id.g88f412eeb7_1_13) from slide 26- 35. Additionally, this matrix can facilitate: 
* [Career Development Conversations](https://about.gitlab.com/handbook/people-group/leadership-toolkit/career-development-conversations/)
* Promotion planning 

## Timeline

### Regular Cadence

Our e-group completes Performance/Potential Matrix quarterly for all their direct reports. The rest of GitLab does this twice annually, ideally in Q2 and Q4. In Q2, the exercise would ideally take place after the [360 annual feedback review](/handbook/people-group/360-feedback/), and in Q4 the exercise would ideally take place in November prior to the [annual compensation review](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review). 

### Initial Launch Timeline

GitLab is kicking off the Performance/Potential Matrix in FY'21 Q3 in preparation for the [annual review](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review) in FY'21 Q4. The timeline for launch, training, and completion of the exericse is as follows:

* 2020-09-30: Review and iterate based on feedback provided in the [issue](https://gitlab.com/gitlab-com/people-group/General/-/issues/965)
* 2020-09-30 (or prior): Update [compensation calculator](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/calculator/) in preparation for October training
* 2020-10-07: Handbook fully updated with first iteration of Performance definitions
* 2020-10-12: Meaning of "potential" defined
* 2020-10-14: Determine training plan and rollout with L&D
* 2020-10-16: Communication to the whole organization
* 2020-10-19: Kick off AMA's/manager training 
* 2020-10-21: First version of the Performance/Potential matrix tool ready
* 2020-10-30: Manager training completed
* 2020-11-01: Kick off formal assessment period
* 2020-11: Performance/potential matrix is completed company wide
* 2020-11-30: Performance rating finalized for all team members and approved through the e-group level
* 2020-12: Individual results of Performance/Potential matrix communicated directly with team member

### Matrix Assessment Timeline 

The Performance/Potential Matrix typically takes 4-6 weeks to complete from beginning to end. Steps are as follows:

1. Managers complete a Performance/Potential Matrix for their respective teams
1. Live session takes place for calibration 
1. Executive Review (leadership meets to review the matrix results, promotion requests, development and performance actions, review discrepancies, and measure against previous matrix results).
1. After finalized, results are shared with team members. 
1. Retrospace takes place with leadership to determine next steps and iterate. 

## Communication 

In most companies the Performance/Potential Matrx is used exclusively as a management tool and results are not typically shared with team members. In the spirit of our transparency value, managers are encouraged to communicate matrix results with team members. We recommend communicating for several reasons:
* To stay true to our values
* This information should not come as a surprise to team members (as career development conversations and feedback should be ongoing)
* It provides an opportunity to receive input/feedback from team members
* It serves as a platform that can feed into a development plan

With the communication consider the following in the light of their performance and potential:

* Refrain from saying things such as: "You're a box X", rather discuss the themes/examples that led to the placement
* Discuss areas the team member is doing well
* Discuss areas that need improvement
* Provide examples
* Provide recommendations for future development
* Prepare the communication in advance

_Note: For Q4 matrices, results should only be communicated to team members after finalized and approved through the e-group level._

## Succession Planning

The succession planning process starts with leaders doing a [performance/potential matrix](/handbook/people-group/performance-assessments-and-succession-planning/#the-performancepotential-matrix) of each of their direct reports.

The resulting charts are reviewed with peers, e.g. all other executives, all other senior leaders in the function, etc.

| Person    | Jane Doe | John Doe |
|-----------|---|---|
| Role      | Job Title  | Job Title  |
| Emergency | Someone who could take over this role if the current person were affected by a [lottery factor](/handbook/total-rewards/compensation/#competitive-rate) or had to take emergency leave |   |
| Ready Now | Someone who could be promoted into the role today  |   |
| 1-2 Years | Someone who could be trained and elevated into the role in 1-2 years  |   |
| 3-5 Years | Someone who could be trained and elevated into the role in 3-5 years  |   |



